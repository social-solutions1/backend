<?php

use AvtoDev\JsonRpc\RpcRouter;
use App\Rpc\Controllers\EventsController;
use App\Rpc\Controllers\ProfileController;

/**
 * Profile Namespace.
 *
 * Auth routes.
 */
RpcRouter::on('auth.login', ProfileController::class . '@login');
RpcRouter::on('auth.register', ProfileController::class . '@register');

/**
 * Profile routes.
 */
RpcRouter::on('profile.me', ProfileController::class . '@me');
RpcRouter::on('email.check', ProfileController::class . '@checkEmail');

/**
 * Event routes.
 */
RpcRouter::on('events.list', EventsController::class . '@search');
RpcRouter::on('events.recommendations', EventsController::class . '@getRecommendations');
RpcRouter::on('events.favorites.add', EventsController::class . '@favoritesAdd');
RpcRouter::on('events.favorites.delete', EventsController::class . '@favoritesDelete');
RpcRouter::on('events.favorites.list', EventsController::class . '@getFavoritesList');
