<?php

use Illuminate\Support\Str;

function parseEvents(): void
{
    $files = \scandir('mock/original_events');
    foreach ($files as $file_name) {
        if (Str::startsWith($file_name, 'events_page_')) {
            $items = \json_decode(\file_get_contents('mock/original_events/' . $file_name), true, 512,
                \JSON_THROW_ON_ERROR);
            foreach ($items as $base_item) {
                if (\is_int($base_item['id'])) {
                    $transfer_data = [
                        'external_id'      => $base_item['id'],
                        'lang'             => $base_item['lang'] ?? 'ru',
                        'title'            => $base_item['title'] ?? '',
                        'text'             => $base_item['text'] ?? '',
                        'date_from'        => $base_item['date_from'] ?? '',
                        'date_to'          => $base_item['date_to'] ?? '',
                        'date'             => $base_item['date'] ?? '',
                        'status'           => $base_item['status'] ?? '',
                        'free'             => (bool) $base_item['free'],
                        'original_content' => \json_encode($base_item, \JSON_THROW_ON_ERROR),
                        'spheres'          => \json_encode($base_item['spheres'] ?? [], \JSON_THROW_ON_ERROR),
                        'kind'             => \json_encode($base_item['kind'] ?? [], \JSON_THROW_ON_ERROR),
                        'restriction'      => \json_encode($base_item['restriction'] ?? [], \JSON_THROW_ON_ERROR),
                        'icon'             => \json_encode($base_item['icon'] ?? [], \JSON_THROW_ON_ERROR),
                        'tags'             => \json_encode($base_item['tags'] ?? [], \JSON_THROW_ON_ERROR),
                        'themes'           => \json_encode($base_item['themes'] ?? [], \JSON_THROW_ON_ERROR),
                        'auditories'       => \json_encode($base_item['auditories'] ?? [], \JSON_THROW_ON_ERROR),
                        'url'              => \json_encode($base_item['auditories'] ?? [], \JSON_THROW_ON_ERROR),
                        'districts'        => \json_encode($base_item['districts'] ?? [], \JSON_THROW_ON_ERROR),
                        'images'           => \json_encode($base_item['images'] ?? [], \JSON_THROW_ON_ERROR),
                        'attach'           => \json_encode($base_item['foundation'] ?? [], \JSON_THROW_ON_ERROR),
                    ];

                    App\Models\Event::create($transfer_data);
                }
            }
        }
    }
}
