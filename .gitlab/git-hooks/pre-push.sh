#!/usr/bin/env sh
set -e;

# Install this hook using next command: `make git-hooks`

if ! make test-fast; then
  printf "\n   \e[39;41m %s \033[0m\n\n" 'Tests failed. For skipping - use "git ... --no-verify" argument' >&2;
  exit 1;
fi;
