<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var string[]
     */
    protected $except = [
        'rpc.endpoint',
    ];

    /**
     * {@inheritdoc}
     *
     * Override for a working with routes NAMES (not URLs).
     */
    protected function inExceptArray($request): bool
    {
        foreach ($this->except as $except) {
            $except = route($except);

            /** @var Request $request */
            if ($request->fullUrlIs($except) || $request->is($except)) {
                return true;
            }
        }

        return false;
    }
}
