<?php

declare(strict_types = 1);

namespace App\Http\Middleware;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Services\Jwt\JwtServiceInterface;
use App\Services\ClientsToken\ClientsTokenServiceInterface;

class CheckJWTTokenMiddleware
{
    /**
     * @var ClientsTokenServiceInterface
     */
    protected $clients_token_service;

    /**
     * @var Guard
     */
    protected $guard;

    /**
     * @var JwtServiceInterface
     */
    private JwtServiceInterface $jwt_service;

    /**
     * CheckJWTTokenMiddleware constructor.
     *
     * @param ClientsTokenServiceInterface $clients_token_service
     * @param Guard                        $guard
     * @param JwtServiceInterface          $jwt_service
     */
    public function __construct(
        ClientsTokenServiceInterface $clients_token_service,
        Guard $guard,
        JwtServiceInterface $jwt_service
    ) {
        $this->clients_token_service = $clients_token_service;
        $this->guard                 = $guard;
        $this->jwt_service           = $jwt_service;
    }

    /**
     * @param Request  $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, \Closure $next)
    {
        $token = $request->bearerToken();

        if (\is_string($token)) {
            $uuid = $this->jwt_service::getUuidFromPayload($token);

            if (\is_string($uuid)) {
                $user = User::where('uuid', $uuid)->first();
                if ($user instanceof User) {
                    $this->guard->setUser($user);
                }
            }
        }

        return $next($request);
    }
}
