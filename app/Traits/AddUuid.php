<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait AddUuid
{
    protected static function boot(): void
    {
        parent::boot();

        static::creating(function (self $item): void {
            if (\property_exists($item, 'uuid')) {
                $item->uuid = (string) Str::uuid();
            }
        });
    }
}
