<?php

declare(strict_types = 1);

namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Authenticatable;

trait AuthUserUuid
{
    /**
     * Возвращаем UUID авторизованного пользователя.
     *
     * @throws AuthenticationException
     *
     * @return string
     */
    public function getAuthUserUuid(): string
    {
        $user = Auth::user();

        if ($user instanceof Authenticatable) {
            return $user->getAuthIdentifier();
        }

        throw new AuthenticationException('User not authenticated.');
    }
}
