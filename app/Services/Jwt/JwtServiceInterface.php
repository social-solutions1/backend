<?php

declare(strict_types = 1);

namespace App\Services\Jwt;

interface JwtServiceInterface
{
    /**
     * @param string $jwt
     * @param string $secret
     *
     * @return bool
     */
    public static function verify(string $jwt, string $secret): bool;

    /**
     * @param string $client_storage_id
     * @param string $secret
     *
     * @return string
     */
    public static function createToken(string $client_storage_id, string $secret): string;

    /**
     * @param string $jwt
     *
     * @return string|null
     */
    public static function getUuidFromPayload(string $jwt): ?string;
}
