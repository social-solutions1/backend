<?php

declare(strict_types = 1);

namespace App\Services\Jwt;

use Carbon\Carbon;
use Firebase\JWT\JWT;

class JwtService implements JwtServiceInterface
{
    /**
     * Algorithm for JWT.
     */
    protected const ALGORITHM = 'HS256';

    /**
     * Verify client JWT token.
     *
     * @param string $jwt
     * @param string $secret
     *
     * @return bool
     */
    public static function verify(string $jwt, string $secret): bool
    {
        try {
            $result = JWT::decode($jwt, $secret, [static::ALGORITHM]);

            return \property_exists($result, 'uuid');
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * Create new JWT token for current client.
     *
     * @param string $client_storage_id
     * @param string $secret
     *
     * @return string
     */
    public static function createToken(string $client_storage_id, string $secret): string
    {
        return JWT::encode([
            'exp'  => Carbon::now()->addWeek()->getTimestamp(),
            'uuid' => $client_storage_id,
        ], $secret, static::ALGORITHM);
    }

    /**
     * Get client UUID from payload.
     *
     * @param string $jwt
     *
     * @return string|null
     */
    public static function getUuidFromPayload(string $jwt): ?string
    {
        try {
            $jwt = \explode('.', $jwt);

            $payload = JWT::jsonDecode(JWT::urlsafeB64Decode($jwt[1] ?? ''));

            if (\property_exists($payload, 'uuid') && \is_string($payload->uuid)) {
                return $payload->uuid;
            }
        } catch (\DomainException $exception) {
            return null;
        }

        return null;
    }
}
