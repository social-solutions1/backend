<?php

namespace App\Services;

use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;

class MosRuParser
{
    private ClientInterface $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $uri
     *
     * @throws GuzzleException
     */
    public function get(string $uri): ResponseInterface
    {
        return $this->client->request('GET', $uri);
    }
}
