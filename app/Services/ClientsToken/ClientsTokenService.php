<?php

declare(strict_types = 1);

namespace App\Services\ClientsToken;

use Illuminate\Redis\Connections\Connection;

class ClientsTokenService implements ClientsTokenServiceInterface
{
    /**
     * @var \Redis
     */
    protected $client;

    /**
     * CountersService constructor.
     *
     * @param Connection $connection
     *
     * @throws \LogicException
     */
    public function __construct(Connection $connection)
    {
        $this->client = $connection->client();

        if (! $this->client instanceof \Redis) {
            throw new \LogicException(
                'Wrong Redis client instance. Actual: ' . \get_class($this->client) . ', required: ' . \Redis::class
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function store(string $client_storage_id, string $token): bool
    {
        return $this->client->setex($client_storage_id, 86400 * 7, $token);
    }

    /**
     * @inheritdoc
     */
    public function get(string $client_storage_id): ?string
    {
        return ($value = $this->client->get($client_storage_id)) !== false
            ? (string) $value
            : null;
    }

    /**
     * @inheritdoc
     */
    public function delete(string $client_storage_id): bool
    {
        return (bool) $this->client->del($client_storage_id);
    }
}
