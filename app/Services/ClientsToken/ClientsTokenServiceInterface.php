<?php

namespace App\Services\ClientsToken;

interface ClientsTokenServiceInterface
{
    /**
     * Store client token.
     *
     * @param string $client_storage_id
     * @param string $token
     *
     * @return bool
     */
    public function store(string $client_storage_id, string $token): bool;

    /**
     * Get client token by client UUID.
     *
     * @param string $client_storage_id
     *
     * @return string|null
     */
    public function get(string $client_storage_id): ?string;

    /**
     * Delete client token by client UUID.
     *
     * @param string $client_storage_id
     *
     * @return bool
     */
    public function delete(string $client_storage_id): bool;
}
