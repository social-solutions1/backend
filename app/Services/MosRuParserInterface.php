<?php

namespace App\Services;

use Psr\Http\Message\ResponseInterface;

interface MosRuParserInterface
{
    /**
     * @param string $uri
     *
     * @return ResponseInterface
     */
    public function get(string $uri): ResponseInterface;
}
