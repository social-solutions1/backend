<?php

declare(strict_types = 1);

namespace App\Services\SpecsService\Exceptions;

class JsonSchemaException extends \RuntimeException
{
}
