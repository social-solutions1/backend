<?php

namespace App\Services\SpecsService;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Services\SpecsService\Exceptions\JsonSchemaException;

class JsonSchema
{
    /**
     * @var string Директория в которой находятся файлы с JSON-схемами
     */
    private $schema_directory;

    /**
     * @var string[] Список всех JSON-схем для запросов/ответов
     */
    private $schemas;

    /** ]
     * JsonSchema constructor.
     *
     * @param string $schema_absolute_path Абсолютный путь до JSON-схемы
     */
    public function __construct(string $schema_absolute_path)
    {
        $this->schema_directory = $schema_absolute_path;

        $all_paths      = DirScanner::scanDir($this->schema_directory);
        $filtered_paths = $this->getOnlyRequestResponsePaths($all_paths);
        $this->schemas  = $this->getPathsWithKeys($filtered_paths);
    }

    /**
     * Возвращаем директорию в которой находятся файлы с JSON-схемами.
     *
     * @return string
     */
    public function getSchemaDir(): string
    {
        return $this->schema_directory;
    }

    /**
     * Получаем массив всех схем для запросов/ответов.
     *
     * @return array|string[]
     */
    public function getAllSchemas(): array
    {
        return $this->schemas;
    }

    /**
     * Возвращаем путь до файла с JSON-схемой.
     *
     * @param string $schema_name
     *
     * @throws JsonSchemaException
     *
     * @return string
     */
    public function pathToSchema(string $schema_name): string
    {
        if (isset($this->schemas[$schema_name])) {
            $path_to_schema = $this->schemas[$schema_name];

            if (\file_exists($path_to_schema)) {
                return $path_to_schema;
            }
        }

        throw new JsonSchemaException("JSON-schema name not exists [name {$schema_name}]");
    }

    /**
     * Фильтруем список файлов, оставляем только схемы для запросов/ответов.
     *
     * @param array|string[] $all_paths
     *
     * @return array|string[]
     */
    private function getOnlyRequestResponsePaths(array $all_paths): array
    {
        return Arr::where($all_paths, function ($value, $key) {
            return Str::endsWith($value, 'params.json') || Str::endsWith($value, 'response.json');
        });
    }

    /**
     * Проставляем в качестве ключей имема файлов (без расширения .json).
     *
     * Получаем массив с элементами вида:
     * 'tags.delete.params' => 'path/schema/tags/delete/tags.delete.params.json'
     *
     * @param array|string[] $paths
     *
     * @return array|string[]
     */
    private function getPathsWithKeys(array $paths): array
    {
        $paths_with_keys = [];
        foreach ($paths as $key => $path) {
            $key                   = \basename($path, '.json');
            $paths_with_keys[$key] = $path;
        }

        return $paths_with_keys;
    }
}
