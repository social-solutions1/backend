<?php

namespace App\Services\SpecsService;

use Illuminate\Support\Facades\File;

class DirScanner
{
    /**
     * Получаем список всех файлов в заданной директории (включая поддиректории).
     *
     * @param string $dir_path
     *
     * @return string[]
     */
    public static function scanDir(string $dir_path): array
    {
        $files = [];
        foreach (File::allFiles($dir_path) as $file) {
            $files[] = $file->getPathname();
        }

        return $files;
    }
}
