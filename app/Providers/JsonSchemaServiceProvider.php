<?php

namespace App\Providers;

use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use App\Services\SpecsService\JsonSchema as JsonSchemaService;

class JsonSchemaServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(JsonSchemaService::class, function (Application $application) {
            return new JsonSchemaService($application->basePath('schemas/schema'));
        });
    }
}
