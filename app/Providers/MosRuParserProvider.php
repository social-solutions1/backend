<?php

namespace App\Providers;

use GuzzleHttp\Client;
use App\Services\MosRuParser;
use App\Services\MosRuParserInterface;
use Illuminate\Support\ServiceProvider;

class MosRuParserProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(MosRuParserInterface::class, static function (): MosRuParser {
            return new MosRuParser(new Client());
        });
    }
}
