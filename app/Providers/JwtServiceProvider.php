<?php

declare(strict_types = 1);

namespace App\Providers;

use App\Services\Jwt\JwtService;
use Illuminate\Support\ServiceProvider;
use App\Services\Jwt\JwtServiceInterface;

class JwtServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(JwtServiceInterface::class, static function (): JwtService {
            return new JwtService;
        });
    }
}
