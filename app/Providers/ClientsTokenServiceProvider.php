<?php

declare(strict_types = 1);

namespace App\Providers;

use Illuminate\Redis\RedisManager;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Container\Container;
use App\Services\ClientsToken\ClientsTokenService;
use App\Services\ClientsToken\ClientsTokenServiceInterface;

class ClientsTokenServiceProvider extends ServiceProvider
{
    /**
     * Connection name for tokens.
     */
    public const
        CONNECTION_NAME = 'tokens-connection';

    /**
     * @inheritdoc
     */
    public function register(): void
    {
        $this->app->bind(ClientsTokenServiceInterface::class,
            static function (Container $container): ClientsTokenServiceInterface {
                /** @var RedisManager $manager */
                $manager = $container->make(RedisManager::class);

                return new ClientsTokenService($manager->connection(static::CONNECTION_NAME));
            });
    }
}
