<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static Model|Builder create($attributes = [])
 * @method static void insert(array $values)
 * @method static Builder find($id, $columns = ['*'])
 * @method static static firstOrCreate(array $attributes, array $values = [])
 * @method static static firstOrNew(array $attributes, array $values = [])
 * @method static static findOrFail($id, $columns = ['*'])
 * @method static Builder inRandomOrder($seed = '')
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and')
 * @method static Builder orderBy($column, $direction = 'asc')
 * @method static Builder forPage($page, $perPage = 15)
 * @method static Builder truncate()
 * @method static Builder whereIn($column, $values)
 * @method static Builder with(...$relations)
 * @method static Builder select($attributes = ['*'])
 * @method static Builder onlyTrashed()
 * @method static Builder whereDate($column, $operator, $value = null, $boolean = 'and')
 * @method static Builder whereDay($column, $operator, $value = null, $boolean = 'and')
 * @method static Builder groupBy(...$groups)
 */
abstract class AbstractModel extends Model
{
}
