<?php

namespace App\Models;

use App\Traits\AddUuid;
use Illuminate\Database\Query\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property int    $id
 * @property string $email
 * @property string $name
 * @property string $uuid
 *
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and')
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable, AddUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var string[]
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var string[]
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return string
     */
    public function getAuthIdentifier(): string
    {
        return $this->uuid;
    }
}
