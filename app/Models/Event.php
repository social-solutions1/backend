<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int    $id
 * @property int    $external_id
 * @property string $lang
 * @property string $title
 * @property string $text
 * @property string $date_from
 * @property string $date_to
 * @property string $date
 * @property string $status
 * @property bool $free
 * @property string $original_content
 * @property string $spheres
 * @property string $kind
 * @property string $restriction
 */
class Event extends AbstractModel
{
    use HasFactory;

    protected $fillable = [
        'external_id',
        'lang',
        'title',
        'text',
        'date_from',
        'date_to',
        'date',
        'status',
        'free',
        'original_content',
        'spheres',
        'kind',
        'restriction',
        'icon',
        'tags',
        'themes',
        'auditories',
        'url',
        'districts',
        'images',
        'attach',
    ];
}
