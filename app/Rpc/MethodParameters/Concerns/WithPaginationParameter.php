<?php

declare(strict_types = 1);

namespace App\Rpc\MethodParameters\Concerns;

use App\Rpc\Entities\Pagination;
use App\Rpc\Entities\PaginationInterface;

trait WithPaginationParameter
{
    /**
     * @var PaginationInterface|null
     */
    protected ?PaginationInterface $pagination = null;

    /**
     * @return PaginationInterface
     */
    public function getPagination(): PaginationInterface
    {
        if (! isset($this->pagination) || ! $this->pagination instanceof Pagination) {
            $this->pagination = $this->extractPaginationParameter($this->params);
        }

        return $this->pagination;
    }

    /**
     * Extract page and on page count for pagination.
     *
     * @param array<string, mixed> $params
     */
    protected function extractPaginationParameter(array $params): Pagination
    {
        $pagination_params = $params['pagination'] ?? [];

        if (\is_object($pagination_params)) {
            $pagination_params = \get_object_vars($pagination_params);
        }

        $page  = $pagination_params['page'] ?? null;
        $limit = $pagination_params['limit'] ?? null;

        return new Pagination($page, $limit);
    }
}
