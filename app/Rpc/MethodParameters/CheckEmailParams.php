<?php

namespace App\Rpc\MethodParameters;

class CheckEmailParams extends BaseParameters
{
    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->params['email'];
    }

    /**
     * @inheritDoc
     */
    protected function getSchema(): string
    {
        return 'email.check.params';
    }
}
