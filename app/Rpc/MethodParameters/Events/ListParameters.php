<?php

declare(strict_types = 1);

namespace App\Rpc\MethodParameters\Events;

use App\Rpc\MethodParameters\BaseParameters;
use App\Rpc\MethodParameters\Concerns\WithPaginationParameter;

class ListParameters extends BaseParameters
{
    use WithPaginationParameter;

    /**
     * @inheritDoc
     */
    protected function getSchema(): string
    {
        return 'events.list.params';
    }
}
