<?php

declare(strict_types = 1);

namespace App\Rpc\MethodParameters;

use JsonSchema\Validator;
use Illuminate\Support\Facades\Request;
use App\Services\SpecsService\JsonSchema;
use App\Rpc\Exceptions\ValidationException;
use AvtoDev\JsonRpc\MethodParameters\MethodParametersInterface;

/**
 * Базовый класс параметров, передаваемых в контроллеры.
 */
abstract class BaseParameters implements MethodParametersInterface
{
    /**
     * Параметры, переданные в запрос.
     *
     * @var array<string, mixed>
     */
    protected $params = [];

    /**
     * @var JsonSchema
     */
    protected $json_schema_service;

    public function __construct(JsonSchema $json_schema_service)
    {
        $this->json_schema_service = $json_schema_service;
        if (\count($this->params) === 0) {
            $this->params = Request::toArray()['params'] ?? [];
        }
    }

    /**
     * Parse passed into method parameters.
     *
     * IMPORTANT: This method will be called automatically by RPC router before injecting into controller.
     *
     * @param array<string,array|int|string|null>|object $params
     *
     * @return void
     */
    public function parse($params): void
    {
        if ($this->validateParams($params)) {
            if (\is_object($params)) {
                $params = \get_object_vars($params);
            }

            $this->params = (array) $params;
        }
    }

    /**
     * Проверяем, что параметры соответствуют JSON-схеме.
     *
     * @param mixed $val
     *
     * @throws ValidationException
     *
     * @return bool
     */
    public function validateParams($val): bool
    {
        if ($val === null) {
            throw new ValidationException;
        }

        $val = (object) $val;

        $validator = new Validator;
        $validator->validate($val,
            (object) ['$ref' => 'file://' . $this->json_schema_service->pathToSchema($this->getSchema())]);

        if ($validator->isValid()) {
            return true;
        }

        throw new ValidationException($validator->getErrors());
    }

    /**
     * Схема, с помощью которой валидирются параметры запроса.
     *
     * @return string
     */
    abstract protected function getSchema(): string;
}
