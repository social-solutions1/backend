<?php

declare(strict_types = 1);

namespace App\Rpc\MethodParameters\Auth;

use App\Rpc\MethodParameters\BaseParameters;

class RegisterParameters extends BaseParameters
{
    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->params['email'];
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->params['password'];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->params['name'];
    }

    /**
     * @inheritdoc
     */
    protected function getSchema(): string
    {
        return 'auth.register.params';
    }
}
