<?php

declare(strict_types = 1);

namespace App\Rpc\MethodParameters\Auth;

use App\Rpc\MethodParameters\BaseParameters;

class LoginParameters extends BaseParameters
{
    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->params['email'];
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->params['password'];
    }

    /**
     * @inheritdoc
     */
    protected function getSchema(): string
    {
        return 'auth.login.params';
    }
}
