<?php

declare(strict_types = 1);

namespace App\Rpc\Controllers;

use App\Models\Event;
use Illuminate\Support\Facades\Log;
use App\Rpc\Entities\EventsListItem;
use App\Rpc\MethodParameters\Events\ListParameters;
use App\Rpc\ResponseObjects\Events\ListResponseObject;

class EventsController
{
    /**
     * Search events.
     *
     * @param ListParameters $parameters
     *
     * @throws \JsonException
     *
     * @return ListResponseObject
     */
    public function search(ListParameters $parameters): ListResponseObject
    {
        $events = Event::query()->simplePaginate($parameters->getPagination()->getLimit(), page:
            $parameters->getPagination()->getPage());
        if ($events->isNotEmpty()) {
            /** @var Event $event */
            foreach ($events->items() as $event) {
                $result[] = new EventsListItem(\json_decode($event->original_content, true, 512, \JSON_OBJECT_AS_ARRAY | \JSON_THROW_ON_ERROR));
            }
        }

        return new ListResponseObject($result ?? [], $parameters->getPagination());
    }

    /**
     * Get events recomendations.
     *
     * @param ListParameters $parameters
     *
     * @throws \JsonException
     *
     * @return ListResponseObject
     */
    public function getRecommendations(ListParameters $parameters): ListResponseObject
    {
        $json = (string) \file_get_contents(base_path('mock/events-list.json'));
        $data = \json_decode($json, true, 512, \JSON_OBJECT_AS_ARRAY | \JSON_THROW_ON_ERROR);
        foreach ($data['items'] as $item) {
            $result[] = new EventsListItem($item);
        }

        return new ListResponseObject($result ?? [], $parameters->getPagination());
    }

    /**
     * @param ListParameters $parameters
     *
     * @throws \JsonException
     *
     * @return ListResponseObject
     */
    public function getFavoritesList(ListParameters $parameters): ListResponseObject
    {
        $json = (string) \file_get_contents(base_path('mock/events-list.json'));
        $data = \json_decode($json, true, 512, \JSON_OBJECT_AS_ARRAY | \JSON_THROW_ON_ERROR);
        foreach ($data['items'] as $item) {
            $result[] = new EventsListItem($item);
        }

        return new ListResponseObject($result ?? [], $parameters->getPagination());
    }

    public function favoritesDelete(): void
    {
        Log::info('test');
    }

    public function favoritesAdd(): void
    {
        Log::info('test');
    }
}
