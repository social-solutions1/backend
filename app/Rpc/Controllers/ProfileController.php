<?php

declare(strict_types = 1);

namespace App\Rpc\Controllers;

use App\Models\User;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Str;
use App\Traits\AuthUserUuid;
use Illuminate\Http\Request;
use App\Services\Jwt\JwtService;
use Illuminate\Hashing\HashManager;
use App\Rpc\Exceptions\RpcException;
use Illuminate\Auth\AuthenticationException;
use App\Rpc\MethodParameters\CheckEmailParams;
use App\Rpc\MethodParameters\Auth\LoginParameters;
use App\Rpc\MethodParameters\Auth\RegisterParameters;
use App\Rpc\ResponseObjects\Client\UserResponseObject;
use App\Services\ClientsToken\ClientsTokenServiceInterface;
use App\Rpc\Exceptions\ValidationException as RpcValidationException;

class ProfileController
{
    use AuthUserUuid;

    /**
     * Give access for Client.
     *
     * @param LoginParameters              $parameters
     * @param ClientsTokenServiceInterface $clients_token_service
     * @param HashManager                  $hash_manager
     *
     * @throws RpcException
     *
     * @return UserResponseObject
     */
    public function login(
        LoginParameters $parameters,
        ClientsTokenServiceInterface $clients_token_service,
        HashManager $hash_manager
    ): UserResponseObject {
        $user = User::where('email', $parameters->getEmail())->first();
        if ($user instanceof User) {
            if ($hash_manager->check($parameters->getPassword(), $user->getAuthPassword())) {
                $clients_token_service->store(
                    $client_storage_id = $user->uuid,
                    $secret = Uuid::uuid4()->toString()
                );

                $client_token = JwtService::createToken($client_storage_id, $secret);

                return new UserResponseObject($user, $client_token);
            }

            throw new RpcException('User data is not valid', 21002);
        }

        throw new RpcException('User not found', 21001);
    }

    /**
     * Register new client.
     *
     * @param RegisterParameters           $parameters
     * @param ClientsTokenServiceInterface $clients_token_service
     * @param HashManager                  $hash_manager
     *
     * @throws \Exception
     * @throws RpcValidationException
     * @throws \InvalidArgumentException
     *
     * @return UserResponseObject
     */
    public function register(
        RegisterParameters $parameters,
        ClientsTokenServiceInterface $clients_token_service,
        HashManager $hash_manager
    ): UserResponseObject {
        if ((User::where('email', $parameters->getEmail())->first()) instanceof User) {
            throw new RpcValidationException(['Пользователь уже существует']);
        }
        $user = (new User)->forceFill([
            'uuid'     => (string) Str::uuid(),
            'name'     => $parameters->getName(),
            'email'    => $parameters->getEmail(),
            'password' => $hash_manager->make($parameters->getPassword()),
        ]);

        if ($user->save()) {
            $clients_token_service->store(
                $client_storage_id = $user->uuid,
                $secret = Uuid::uuid4()->toString()
            );
            $client_token = JwtService::createToken($client_storage_id, $secret);

            return new UserResponseObject($user, $client_token);
        }

        throw new RpcValidationException(['Не удалось создать пользователя']);
    }

    /**
     * @param Request $request
     *
     * @throws AuthenticationException
     *
     * @return UserResponseObject
     */
    public function me(Request $request): UserResponseObject
    {
        $user = User::where('uuid', $this->getAuthUserUuid())->first();
        if (! $user instanceof User) {
            throw new RpcException('User not found', 21001);
        }

        return new UserResponseObject($user, (string) $request->bearerToken());
    }

    /**
     * @param CheckEmailParams $parameters
     *
     * @return array{found: bool}
     */
    public function checkEmail(CheckEmailParams $parameters): array
    {
        return [
            'found' => User::where('email', $parameters->getEmail())->first() instanceof User,
        ];
    }
}
