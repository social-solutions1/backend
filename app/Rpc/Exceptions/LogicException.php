<?php

declare(strict_types = 1);

namespace App\Rpc\Exceptions;

class LogicException extends RpcException
{
}
