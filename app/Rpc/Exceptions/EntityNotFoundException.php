<?php

declare(strict_types = 1);

namespace App\Rpc\Exceptions;

class EntityNotFoundException extends RpcException
{
}
