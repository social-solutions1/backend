<?php

declare(strict_types = 1);

namespace App\Rpc\ResponseObjects\Events;

use App\Rpc\Entities\EventsListItem;
use App\Rpc\Entities\PaginationInterface;
use App\Rpc\ResponseObjects\ResponseObjectInterface;
use App\Rpc\ResponseObjects\Pagination\PaginationResponseObject;

class ListResponseObject implements ResponseObjectInterface
{
    /**
     * @var array<EventsListItem>
     */
    private array $events;

    /**
     * @var PaginationInterface
     */
    private PaginationInterface $pagination;

    /**
     * @param array<EventsListItem> $events
     * @param PaginationInterface   $pagination
     */
    public function __construct(array $events, PaginationInterface $pagination)
    {
        $this->events     = $events;
        $this->pagination = $pagination;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'pagination' => new PaginationResponseObject($this->pagination),
            'events'     => \count($this->events) > 0
                ? \array_map(static function (EventsListItem $item): array {
                    return $item->toArray();
                }, $this->events)
                : [],
        ];
    }
}
