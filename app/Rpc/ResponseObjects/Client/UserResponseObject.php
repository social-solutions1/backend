<?php

declare(strict_types = 1);

namespace App\Rpc\ResponseObjects\Client;

use App\Models\User;
use App\Rpc\ResponseObjects\ResponseObjectInterface;

class UserResponseObject implements ResponseObjectInterface
{
    /**
     * @var User
     */
    private User $user;

    /**
     * @var string
     */
    private string $token;

    /**
     * @param User   $user
     * @param string $token
     */
    public function __construct(User $user, string $token)
    {
        $this->user  = $user;
        $this->token = $token;
    }

    /**
     * @return array<string, mixed>
     */
    public function jsonSerialize(): array
    {
        return [
            'uuid'       => $this->user->uuid,
            'token'      => $this->token,
            'email'      => $this->user->email,
            'name'       => $this->user->name,
        ];
    }
}
