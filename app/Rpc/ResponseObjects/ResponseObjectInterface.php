<?php

declare(strict_types = 1);

namespace App\Rpc\ResponseObjects;

interface ResponseObjectInterface extends \JsonSerializable
{
}
