<?php

declare(strict_types = 1);

namespace App\Rpc\ResponseObjects\Pagination;

use App\Rpc\Entities\PaginationInterface;
use App\Rpc\ResponseObjects\ResponseObjectInterface;

class PaginationResponseObject implements ResponseObjectInterface
{
    /**
     * @var PaginationInterface
     */
    private $pagination;

    /**
     * @param PaginationInterface $pagination
     */
    public function __construct(PaginationInterface $pagination)
    {
        $this->pagination = $pagination;
    }

    /**
     * @return array<int>
     */
    public function jsonSerialize(): array
    {
        return [
            'page'  => $this->pagination->getPage(),
            'limit' => $this->pagination->getLimit(),
        ];
    }
}
