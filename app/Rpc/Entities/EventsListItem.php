<?php

declare(strict_types = 1);

namespace App\Rpc\Entities;

class EventsListItem
{
    /**
     * @var array<string, mixed>
     */
    private array $data;

    /**
     * @param array<string, mixed> $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return [
            'id'        => $this->data['id'],
            'title'     => $this->data['title'],
            'date_from' => $this->data['date_from'],
            'date_to'   => $this->data['date_to'],
            'free'      => $this->data['free'],
            'kind'      => $this->data['kind'],
            'image'     => [
                'id'    => $this->data['image']['id'],
                'thumb' => $this->data['image']['thumb'],
            ],
            'spheres' => $this->data['spheres'],
        ];
    }
}
