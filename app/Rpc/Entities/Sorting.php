<?php

declare(strict_types = 1);

namespace App\Rpc\Entities;

class Sorting implements SortingInterface
{
    protected const SORT_KEY_CREATED_AT    = 'created_at',
                    SORT_KEY_UPDATED_AT    = 'updated_at',
                    SORT_KEY_LAST_USAGE_AT = 'last_usage_at',
                    SORT_KEY_AUTO_INDEX    = 'auto_index';

    protected const
                    POSSIBLE_SORT_KEYS  = [
                        self::SORT_KEY_CREATED_AT,
                        self::SORT_KEY_UPDATED_AT,
                        self::SORT_KEY_LAST_USAGE_AT,
                        self::SORT_KEY_AUTO_INDEX,
                    ];

    protected const SORT_ORDER_ASC      = 'asc',
                    SORT_ORDER_DESC     = 'desc';

    /**
     * @var string
     */
    protected $sort_key = self::SORT_KEY_CREATED_AT;

    /**
     * @var string
     */
    protected $sort_order = self::SORT_ORDER_ASC;

    public function __construct(?string $sort_key, ?string $sort_order)
    {
        $this->sort_key   = $this->normalizeSortKey($sort_key);
        $this->sort_order = $this->normalizeSortOrder($sort_order);
    }

    /**
     * @inheritdoc
     */
    public function isSortDesc(): bool
    {
        return $this->sort_order === self::SORT_ORDER_DESC;
    }

    /**
     * @inheritdoc
     */
    public function getSortOrder(): string
    {
        return $this->sort_order;
    }

    /**
     * @inheritdoc
     */
    public function getSortKey(): string
    {
        return $this->sort_key;
    }

    /**
     * @param string|null $sort_key
     *
     * @return string
     */
    protected function normalizeSortKey(?string $sort_key): string
    {
        return $sort_key !== null && \in_array($sort_key, self::POSSIBLE_SORT_KEYS)
            ? $sort_key
            : self::SORT_KEY_CREATED_AT;
    }

    /**
     * @param string|null $sort_order
     *
     * @return string
     */
    protected function normalizeSortOrder(?string $sort_order): string
    {
        return $sort_order !== null
               && ($sort_order === self::SORT_ORDER_ASC || $sort_order === self::SORT_ORDER_DESC)
            ? $sort_order
            : self::SORT_ORDER_ASC;
    }
}
