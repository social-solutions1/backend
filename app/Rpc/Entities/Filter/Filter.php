<?php

declare(strict_types = 1);

namespace App\Rpc\Entities\Filter;

use Carbon\Carbon;

class Filter
{
    /**
     * @var GenerationDate
     */
    private $generation_date;

    /**
     * @var array<int>
     */
    private $auto_index;

    /**
     * @var array<string>|null
     */
    private $stages;

    /**
     * @var array<int>|null
     */
    private $tags_ids;

    /**
     * @var array<string>|null
     */
    private $additional_blocks;

    /**
     * @var array<string>|null
     */
    private $uuids;

    /**
     * @var Carbon|null
     */
    private $date_from;

    /**
     * @var Carbon|null
     */
    private $date_to;

    /**
     * Filters constructor.
     *
     * @param array<int> $auto_index
     */
    public function __construct(array $auto_index)
    {
        $this->auto_index      = $auto_index;
    }

    /**
     * @return GenerationDate
     */
    public function getGenerationDate(): GenerationDate
    {
        return new GenerationDate($this->date_from, $this->date_to);
    }

    /**
     * @param Carbon $date
     *
     * @return self
     */
    public function setDateFrom(Carbon $date): self
    {
        $this->date_from = $date;

        return $this;
    }

    /**
     * @param Carbon $date
     *
     * @return self
     */
    public function setDateTo(Carbon $date): self
    {
        $this->date_to = $date;

        return $this;
    }

    /**
     * @return array<int>
     */
    public function getAutoIndex(): array
    {
        return $this->auto_index;
    }

    /**
     * @return array<string>
     */
    public function getStages(): array
    {
        return $this->stages ?? [];
    }

    /**
     * @param array<string> $stages
     *
     * @return self
     */
    public function setStages(array $stages): self
    {
        $this->stages = $stages;

        return $this;
    }

    /**
     * @return array<int>
     */
    public function getTagsIDs(): array
    {
        return $this->tags_ids ?? [];
    }

    /**
     * @param array<int> $tags_ids
     *
     * @return self
     */
    public function setTagsIds(array $tags_ids): self
    {
        $this->tags_ids = $tags_ids;

        return $this;
    }

    /**
     * @return array<string>
     */
    public function getAdditionalBlocks(): array
    {
        return $this->additional_blocks ?? [];
    }

    /**
     * @param array<string> $additional_blocks
     *
     * @return self
     */
    public function setAdditionalBlocks(array $additional_blocks): self
    {
        $this->additional_blocks = $additional_blocks;

        return $this;
    }

    /**
     * @return array<string>
     */
    public function getUuids(): array
    {
        return $this->uuids ?? [];
    }

    /**
     * @param array<string> $uuids
     *
     * @return self
     */
    public function setUuids(array $uuids): self
    {
        $this->uuids = $uuids;

        return $this;
    }
}
