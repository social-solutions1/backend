<?php

declare(strict_types = 1);

namespace App\Rpc\Entities\Filter;

use Carbon\Carbon;

class GenerationDate
{
    /**
     * @var Carbon|null
     */
    private $date_from;

    /**
     * @var Carbon|null
     */
    private $date_to;

    /**
     * Generation date constructor.
     *
     * @param Carbon|null $date_from
     * @param Carbon|null $date_to
     */
    public function __construct(?Carbon $date_from, ?Carbon $date_to)
    {
        $this->date_from = $date_from instanceof Carbon
            ? (clone $date_from)->startOfDay()
            : null;
        $this->date_to   = $date_to instanceof Carbon
            ? (clone $date_to)->endOfDay()
            : null;
    }

    public function from(): ?Carbon
    {
        return $this->date_from;
    }

    public function to(): ?Carbon
    {
        return $this->date_to;
    }
}
