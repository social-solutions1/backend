<?php

namespace App\Rpc\Entities;

interface SortingInterface
{
    /**
     * Get sorting order.
     *
     * @return bool
     */
    public function isSortDesc(): bool;

    /**
     * Returns the sort order of the list (desc, asc).
     *
     * @return string
     */
    public function getSortOrder(): string;

    /**
     * Get sorting key.
     *
     * @return string
     */
    public function getSortKey(): string;
}
