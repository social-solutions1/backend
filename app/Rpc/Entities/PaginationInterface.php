<?php

namespace App\Rpc\Entities;

interface PaginationInterface
{
    /**
     * Get requested page.
     *
     * @return int
     */
    public function getPage(): int;

    /**
     * Get limit items per page.
     *
     * @return int
     */
    public function getLimit(): int;

    /**
     * Get offset it items list.
     *
     * @return int
     */
    public function getOffset(): int;
}
