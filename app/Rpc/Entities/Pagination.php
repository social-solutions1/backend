<?php

declare(strict_types = 1);

namespace App\Rpc\Entities;

class Pagination implements PaginationInterface
{
    protected const
        DEFAULT_LIMIT = 10,
        MIN_LIMIT     = 1,
        MAX_LIMIT     = 50;

    /**
     * Number of first page.
     */
    protected const FIRST_PAGE = 1;

    /**
     * Requested page number.
     *
     * @var int
     */
    protected $page = 1;

    /**
     * @var int
     */
    protected $limit = 10;

    public function __construct(?int $page, ?int $limit)
    {
        $this->page  = $this->normalizePage($page);
        $this->limit = $this->normalizeLimit($limit);
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return ($this->page - 1) * $this->limit;
    }

    /**
     * Make page normalization.
     *
     * @param int|null $page
     *
     * @return int
     */
    protected function normalizePage(?int $page): int
    {
        return ($page !== null && $page >= self::FIRST_PAGE)
            ? $page
            : self::FIRST_PAGE;
    }

    /**
     * Make limit normalization.
     *
     * @param int|null $limit
     *
     * @return int
     */
    protected function normalizeLimit(?int $limit): int
    {
        return ($limit !== null && $limit >= self::MIN_LIMIT && $limit <= self::MAX_LIMIT)
            ? $limit
            : self::DEFAULT_LIMIT;
    }
}
