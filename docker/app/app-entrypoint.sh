#!/usr/bin/env sh
set -e

APP_DIR="${APP_DIR:-/app}";
STARTUP_DELAY="${STARTUP_DELAY:-0}";
STARTUP_WAIT_FOR_SERVICES="${STARTUP_WAIT_FOR_SERVICES:-false}";

if [ "$STARTUP_DELAY" -gt 0 ]; then
  echo "$0: wait $STARTUP_DELAY seconds before starting ..";
  sleep "$STARTUP_DELAY";
fi;

# Wait for services ready state
if [ "$STARTUP_WAIT_FOR_SERVICES" = "true" ]; then
  echo "$0: wait for services ready state";
  counter=0;
  try_limit=60;

  while :; do
    counter=$((counter + 1));

    if [ ${counter} -gt ${try_limit} ]; then
      (>&2 echo "$0: errors limit reached"); exit 1;
    fi;

    ( php "$APP_DIR/artisan" migrate:status ) 2>&1 && break;

    echo "$0: ($counter of $try_limit) wait.."; sleep 1;
  done;
fi;

exec "$@"
