<?php

namespace Tests\Bootstrap;

use App\Console\Kernel;
use Illuminate\Support\Str;
use Tests\CreatesApplication;
use Illuminate\Foundation\Testing\TestCase;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * @coversNothing
 */
class TestsBootstrapper extends TestCase
{
    use CreatesApplication;

    /**
     * @var bool Выключить миграции?
     */
    protected $turn_off_migrations = false;

    /**
     * Bootstrapper constructor.
     *
     * @param bool $turn_off_migrations Выключить миграции?
     *
     * @throws \Exception
     */
    public function __construct(bool $turn_off_migrations = false)
    {
        $this->app                 = $this->createApplication();
        $this->turn_off_migrations = $turn_off_migrations;
        parent::__construct();

        $this->bootNeedMigrateIfNeeded();
    }

    /**
     * Проверяет необходимость выполнения миграций и выполняет их, если это требуется.
     *
     * @return bool
     */
    protected function bootNeedMigrateIfNeeded(): bool
    {
        global $argv;
        static $skip_arguments = ['--group'];

        if ($this->turn_off_migrations !== true) {
            foreach ($argv as $argument) {
                foreach ($skip_arguments as $skip) {
                    if (Str::contains(Str::lower(\trim($argument)), Str::lower($skip))) {
                        $this->log(\sprintf('Skip database refreshing (argument "%s" found)', $argument));

                        return true;
                    }
                }
            }

            $this->refreshDatabase();
        }

        return true;
    }

    /**
     * Выполняет мигрирование БД и наполняет её тестовыми данными.
     */
    protected function refreshDatabase(): void
    {
        /** @var Kernel|null $kernel */
        static $kernel = null;

        if (! ($kernel instanceof Kernel)) {
            $kernel = $this->app->make(Kernel::class);
        }

        $this->log('Refresh migrations..');
        $kernel->call('migrate:refresh');

        $this->log('Apply seeds..');
        $kernel->call('db:seed');
    }

    /**
     * Show "styled" console message.
     *
     * @param string|null $message
     * @param string      $style
     *
     * @return void
     */
    protected function log($message = null, $style = 'info'): void
    {
        /** @var ConsoleOutput|null $output */
        static $output = null;

        if (! ($output instanceof ConsoleOutput)) {
            $output = $this->app->make(ConsoleOutput::class);
        }

        $output->writeln(empty((string) $message)
            ? ''
            : \sprintf('<%1$s>> Bootstrap:</%1$s> <%2$s>%3$s</%2$s>', 'comment', $style, $message)
        );
    }
}
