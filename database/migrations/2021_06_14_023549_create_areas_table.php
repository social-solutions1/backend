<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAreasTable extends Migration
{
    private const TABLE_NAME = 'areas';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(static::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('external_id')->unique();
            $table->string('title');
            $table->string('local_id');
            $table->unsignedBigInteger('district_id');
            $table->timestamps();
        });
        $items = \json_decode(\file_get_contents(base_path('mock/areas/areas.json')), true, 512, JSON_THROW_ON_ERROR);

        foreach ($items as $item) {
            DB::table(static::TABLE_NAME)->insert([
                'external_id' => $item['id'],
                'title' => $item['title'],
                'local_id' => $item['local_id'],
                'district_id' => $item['district_id'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(static::TABLE_NAME);
    }
}
