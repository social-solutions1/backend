<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    protected const TABLE_NAME = 'events';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(static::TABLE_NAME, function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('external_id')->unique();
            $table->string('lang')->default('ru');
            $table->string('title');
            $table->text('text');
            $table->string('date_from');
            $table->string('date_to');
            $table->string('date');
            $table->string('status');
            $table->integer('free');
            $table->jsonb('original_content');
            $table->jsonb('spheres')->nullable();
            $table->jsonb('kind')->nullable();
            $table->jsonb('restriction')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(static::TABLE_NAME);
    }
}
