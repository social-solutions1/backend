<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateSpheresTable extends Migration
{
    private const TABLE_NAME = 'spheres';

    /**
     * Run the migrations.
     *
     * @return void
     * @throws JsonException
     */
    public function up(): void
    {
        Schema::create(static::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('external_id')->unique();
            $table->string('title');
            $table->integer('activated');
            $table->integer('priority');
            $table->timestamps();
        });
        $items = \json_decode(\file_get_contents(base_path('mock/spheres/spheres.json')), true, 512, JSON_THROW_ON_ERROR);

        foreach ($items as $item) {
            DB::table(static::TABLE_NAME)->insert([
                'external_id' => $item['id'],
                'title'       => $item['title'],
                'activated'   => $item['activated'],
                'priority'    => $item['priority'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(static::TABLE_NAME);
    }
}
