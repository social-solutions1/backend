<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToEventsTable extends Migration
{
    protected const TABLE_NAME = 'events';

    protected const NEW_COLUMNS = 'icon,tags,themes,auditories,url,spots,districts,images,attach,foundation';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table): void {
            foreach (\explode(',', static::NEW_COLUMNS) as $column_name) {
                $table->jsonb($column_name)->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table): void {
            foreach (\explode(',', static::NEW_COLUMNS) as $column_name) {
                $table->dropColumn($column_name);
            }
        });
    }
}
